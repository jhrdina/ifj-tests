{ Test funkce sort }
var
	a: integer;
	str: string;

function test(str: string): integer;
var
	len: integer;
begin
	test:= 1;
	len := length(str);
	str := sort(str);
	write(str, ''#10'');
	write(length(str) = len, ''#10'')
end;

begin
	a := test('bcda');
	a := test('ahoj');
	a := test('');

	a:=260;
	str:='';
	while a > 0 do
	begin
		str := str + 'a';
		a := a -1
	end;

	a := test(str);
	write(length(str), ''#10'');
	str:=sort(str);
	write(length(str), ''#10'')

end.
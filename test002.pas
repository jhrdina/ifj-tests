{------------------------ Rozsirenie REPEAT ----------------------------------}
{-------------------------- EXIT_CODE 0 --------------------------------------}
{ Euklidov algoritmus                                                         }

{ Globalne premenne }
var num1 : integer;
	num2 : integer;
	tmp : integer;

{ Funkcia pre vypocet absolutnej hodnoty }
function gabs(a : integer) : integer;
begin
	if a < 0 then
		begin
			a := a * (0 - 1)
		end
	else
		begin
		end; { if-else }
	gabs := a
end;


{ Funkcia pre vypocet celociselneho podielu cisel }
function gdiv(a : integer; b : integer) : integer;
var sign : integer; tmp : integer; 
begin
	if a * b < 0 then
		begin
			sign := 0 - 1
		end
	else
		begin
			sign := 1
		end;

	a := gabs(a);
	b := gabs(b);

	gdiv := 0;
	tmp := b;

	while tmp <= a do
		begin
			tmp := tmp + b;
			gdiv := gdiv + 1
		end;
	gdiv := gdiv * sign
end;

{ Funkcia pre vypocet modulo }
function gmod(a : integer; b : integer) : integer;
begin
	gmod := gdiv(a, b);
	gmod := a - b * gmod
end;

{ Hlavny program }
begin
	write('Program pre vypocet najvacsieho spolocneho delitela'#10'');
	write('---------------------------------------------------'#10'');

	{ Prve cislo }
	num1 := 4221;
	{ Druhe cislo }
	num2 := 42;

	repeat
		begin
		if num1 < num2 then
			begin
				tmp := num1;
				num1 := num2;
				num2 := tmp
			end
		else
			begin
			end;

		num1 := gmod(num1, num2)
		end
	until num1 = 0;

	write(num2, ''#10'')
end.




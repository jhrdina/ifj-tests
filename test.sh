#!/bin/bash
# Spusteni s parametrem "keep" zabrani smazani docasnych souboru s vystupy

BINARY="../ifj/projekt"

if [ `basename "$PWD"` != "ifj-tests" ]; then
	cd "../ifj-tests"
	if [ `basename "$PWD"` != "ifj-tests" ]; then
		echo "Spustte skript ve slozce s projektem nebo ve slozce s testy."
		exit 1
	fi
fi

ok_count=0
count=0

red='\033[1;31m'
green='\033[1;32m'
NC='\033[0m'

keep_files=0
if [ "$1" == "keep" ]; then
	keep_files=1
fi


# Pouziti: run_test <args> <nazev_souboru> <exit_code> <output_file_number> <popis>
#   <args> - argumenty pro spusteni projektu
#   <nazev_souboru> - soubor s testem bez pripony, ocekavaji se soubory:
#                     <nazev_souboru>.pas    v tests
#                     <nazev_souboru>.out v tests/outputs nebo
#                     <nazeb_souboru>-<output_file_number>.out v tests/outputs
#   <exit_code> - ocekavany exit code
#   <output_file_number> - cislo souboru k porovnani (mozno vynechat) - kvuli
#                     pripadum, kdy je jeden zdrojovy kod testovan vickrat
#                     s ruznymi vstupy
#   <popis> - popis testu - zobrazi se ve vypisu
function run_test {
	((count++))

	printf ">>> %-7s %-59s " "$2" "$5"

	"$BINARY" $1 "$2.pas" 1> "$2-$4.ours.out" 2> "$2-$4.ours.errout"
	exit_code="$?"

	out_file="outputs/$2.out"
	if [ "$4" != "" ]; then
		out_file="outputs/$2-$4.out"
	fi

	diffout=`diff "$2-$4.ours.out" "$out_file"`

	if [ "$?" = "0" ] && [ "$exit_code" = "$3" ]; then
		echo -e "[  ${green}OK${NC}  ]"
		((ok_count++))
	else
		echo -e "[ ${red}FAIL${NC} ]"
		echo "Exit code: $exit_code"
		echo "Diff:"
		echo "$diffout"
		echo
	fi

	if [ "$keep_files" == "0" ]; then
		rm "$2-$4.ours.out"
		rm "$2-$4.ours.errout"
	fi
}

################################################################################

# Nova konvence pro nazvy testu - prvni cislice vyjadruji navratovy kod

#      <args> <nazev_souboru> <exit_code> <output_file_number> <popis>

echo
echo -e "  ${red}SCANNER TESTS${NC}  "
echo
run_test "-t" test1  "0" ""  "Function declaration"
run_test "-t" test2  "0" ""  "Keywords"
run_test "-t" test3  "1" ""  "Identificators"
run_test "-t" test4  "0" ""  "Numbers basic"
run_test "-t" test5  "0" ""  "Correct strings"
run_test "-t" test6  "1" ""  "Mathematical chars + others, \ throws ERROR"
run_test "-t" test34 "1" ""  "Symbol ! throws lex ERROR"
run_test "-t" test35 "1" ""  "Symbol @ throws lex ERROR"
run_test "-t" test36 "1" ""  "Symbol # throws lex ERROR"
run_test "-t" test37 "1" ""  "Symbol $ throws lex ERROR"
run_test "-t" test38 "1" ""  "Symbol ^ throws lex ERROR"
run_test "-t" test39 "1" ""  "Symbol & throws lex ERROR"
run_test "-t" test7  "1" ""  "Program code"
run_test "-t" test21 "0" ""  "Keywords case insensitivity"
run_test "-t" test22 "1" ""  "Exp. numbers"
run_test "-t" test23 "1" ""  "Numbers: 20e. throws lex ERROR"
run_test "-t" test24 "1" ""  "Numbers: 20.++ throws lex ERROR"
run_test "-t" test25 "1" ""  "Hashtag escape range"
run_test "-t" test26 "1" ""  "Operators"
run_test "-t" test27 "1" ""  "Numbers: 20.E+14 throws lex ERROR"
run_test "-t" test28 "0" ""  "Transition between number and identificator"
run_test "-t" test29 "1" ""  "Numbers: 20.20E++20 throws lex ERROR"
run_test "-t" test30 "1" ""  "Tab in string throws lex ERROR"
run_test "-t" test31 "0" ""  "Transition between number and dot"
run_test "-t" test32 "1" ""  "'jaja'#256' throws lex ERROR"
run_test "-t" test33 "1" ""  "Not ended comment"
run_test "-t" test50 "1" ""  "String : 'Ahoj'''#33'"
run_test "-t" test51 "1" ""  "String : 'Ahoj''#33''"
run_test "-t" test42 "1" ""  "String : 'Ahoj'#33'''"
run_test "-t" test43 "1" ""  "String : 'Ahoj'#33'"
run_test "-t" test44 "1" ""  "String : '#200''"
run_test "-t" test45 "1" ""  "String : 'IFJ TEST noveho riadku v retazci'''"
run_test "-t" test46 "1" ""  "String : '200"
run_test "-t" test47 "1" ""  "String : 'end"
run_test "-t" test48 "1" ""  "String : '#13"
run_test "-t" test49 "1" ""  "String : 'while + 4 * 8}"

echo
echo -e "  ${green} SYNTAX TESTS${NC}"
echo

run_test "" test201 "2" "" "Definition of global variables after def. of functions"
run_test "" test202 "2" "" "Missing semicolon after definition of global variables"
run_test "" test203 "2" "" "Incomplete definition of variables"
run_test "" test204 "2" "" "After last parameter of function is the semicolon"
run_test "" test205 "2" "" "Missing semicolon after definition of function"
run_test "" test206 "2" "" "Missing body of function"
run_test "" test207 "2" "" "Only the semicolon in compound statement. "
run_test "" test208 "2" "" "Missing expression in 'if' statement"
run_test "" test209 "2" "" "Missing coumpound statement in 'if' statement"
run_test "" test210 "2" "" "Missing expression in 'while' statement"
run_test "" test211 "2" "" "Missing coumpound statement in 'while' statement"
run_test "" test212 "2" "" "More than one parameter in 'readln'"
run_test "" test213 "2" "" "'readln' without parameter"
run_test "" test214 "2" "" "'write' without parameter"
run_test "" test215 "2" "" "The semicolon after last parameter in 'write'"
run_test "" test216 "2" "" "Left operand is not variable in assignment "
run_test "" test217 "2" "" "One more semicolon after statement"
run_test "" test218 "2" "" "Empty program"
run_test "" test219 "2" "" "Key word 'write' in definition of variable"

run_test "" test220 "2" "" "Key word 'write' in definition of function"
run_test "" test221 "2" "" "Missing dot after main program"

echo
echo -e "  ${green}SEMANTIC TESTS WITH ERROR CODE 3${NC}"
echo

run_test "" test301 "3" "" "Redefinition of global variable"
run_test "" test302 "3" "" "Redefinition of local variable"
run_test "" test303 "3" "" "Redefinition of function"
run_test "" test304 "3" "" "Forward declaration after definition of funtion"
run_test "" test305 "3" "" "Different header in forward declaration"
run_test "" test306 "3" "" "Only forward declaration of function"
run_test "" test307 "3" "" "Redefinition of 'sort' function"
run_test "" test308 "3" "" "Redefinition of 'find' function"
run_test "" test309 "3" "" "Redefinition of 'length' function"
run_test "" test310 "3" "" "Redefiniton of 'copy' function"

echo
echo -e "  ${green}SEMANTIC TESTS WITH ERROR CODE 4${NC}"
echo

run_test "" test401 "4" "" "Different data types in assignment"
run_test "" test402 "4" "" "Different data type in 'if' condition"
run_test "" test403 "4" "" "Different data type in 'while' condition"
run_test "" test404 "4" "" "Variable is boolean in 'readln'"
run_test "" test405 "4" "" "Concatenation string + int constant"
run_test "" test406 "4" "" "bool + bool"
run_test "" test407 "4" "" "int <> real"
run_test "" test408 "4" "" "string > int"
run_test "" test409 "4" "" "No implicit conv. in func. call: int -> real"
run_test "" test410 "4" "" "No implicit conv. in func. call: int -> bool"
run_test "" test411 "4" "" "Result of int / int shouldn't be int"
run_test "" test412 "4" "" "Too many parameters in func. call"
run_test "" test413 "4" "" "Missing parameters in func. call"
run_test "" test414 "4" "" "Not enought parameters in func. call"

echo
echo -e "  ${green}INTERPRETER INPUT TESTS WITH ERROR CODE 6${NC}"
echo

echo 'abc'|run_test "" test601 "6" "" "Input is not integer"
echo '12abc'|run_test "" test601 "6" "" "Input is integer with string unseparated"
echo '1.1'|run_test "" test601 "6" "" "Input is real"
echo '\n1'|run_test "" test601 "6" "" "Newline before input number"
echo '10'|run_test "" test601 "0" "" "Input is OK"
echo '-10'|run_test "" test601 "0" "" "Input is NEGATIVE"
echo '-10 abc'|run_test "" test601 "0" "" "Input is OK with string separated"
printf '1\n2\n'|run_test "" test602 "0" "" "Two intput numbers OK"
printf '1 10\n2\n'|run_test "" test602 "0" "" "Two intput numbers with unused number separated"
printf '1 abcdefgh\n2\n'|run_test "" test602 "0" "" "Two intput numbers with unused string separated"

echo

echo 'abc'|run_test "" test603 "6" "" "Input is not real"
echo '3.14abc'|run_test "" test603 "6" "" "Input is real with string unseparated"
echo '3.14+e10'|run_test "" test603 "6" "" "Bad input 3.14+e10"
echo '\n3.14'|run_test "" test603 "6" "" "Newline before input number"
echo '3.14'|run_test "" test603 "0" "" "Input is OK (3.14)"
echo '31.4e1'|run_test "" test603 "0" "" "Input is OK (31.4e1)"
echo '-3.14'|run_test "" test603 "0" "" "Input is NEGATIVE"
echo '-3.14 abc'|run_test "" test603 "0" "" "Input is OK with string separated"
printf '314e-2\n0.42e+2\n'|run_test "" test604 "0" "" "Two intput numbers OK"
printf '3.14 123\n42 efgh\n'|run_test "" test604 "0" "" "Two intput numbers with unused number separated"
printf '3.14 abcd\n42 efgh\n'|run_test "" test604 "0" "" "Two intput numbers with unused string separated"

echo
echo -e "  ${green}INTERPRETER UNINITIALIZED VARIABLE TESTS WITH ERROR CODE 7${NC}"
echo

run_test "" test701 "7" "" "Global variable unicialized"
run_test "" test702 "7" "" "Local variable unicialized"
run_test "" test703 "7" "" "Return value unicialized"
run_test "" test704 "7" "" "Uninit detection in 1000th recursion"
run_test "" test705 "0" "" "Reading from return value of function"


echo
echo -e "  ${green}INTERPRETER ZERO DIV TESTS WITH ERROR CODE 8${NC}"
echo

run_test "" test801 "8" "" "Dividing by 0 (integer)"
run_test "" test802 "8" "" "Dividing by 0 (real)"


echo
echo -e "  ${green}BUILD-IN FUNCTIONS TESTS${NC}"
echo

run_test "" test060 "0" "" "Function length"
run_test "" test061 "0" "" "Function copy"
run_test "" test062 "0" "" "Function find"
run_test "" test063 "0" "" "Function sort"

echo
echo -e "  ${green}OTHER TESTS${NC}"
echo

printf '2\n2\n1' | run_test "" test8  "0" "0" "Vstupy 2 2 1"
printf '3\n43\n51\n12' | run_test "" test8  "0" "1" "Vstupy 3 43 51 12"
run_test "" test9  "0" "" "Test Boolean operaci"
printf "%d\n2\n" "-10" | run_test "" test11 "0" "" "Metoda secen"
echo '1000' | run_test "" test12 "0" "" "Metoda tecen"
printf '4.2\n 20\n 65\nm\n' | run_test "" test13 "0" "" "Vypocet alkoholu v krvi"
run_test "" test14 "0" "" "Test sloziteho vyrazu"
run_test "" test15 "0" "" "Test prace s retezci"
printf "1000\n" | run_test "" test20 "0" "" "Test rekurze"
run_test "" test40 "0" "" "Implicit conversions, operation result types"
run_test "" test004 "0" "" "Negative numbers saving and printing"
echo '7' | run_test "" test005 "0" "" "Minimized prog2 (almost without white symbols)"
run_test "" test006 "0" "" "Forward declaration of function"
run_test "" test011 "0" "" "Finding substring"
echo 
echo -e "  ${red}REPEAT..UNTIL${NC}"
echo
run_test "" test251 "2" "" "Empty body of 'repeat'"
run_test "" test252 "2" "" "The semicolon after last statement in 'repeat'"
run_test "" test253 "2" "" "Missing semicolon after 'repeat' statement"
run_test "" test001 "0" "" "Correct program with 'repeat'"
run_test "" test002 "0" "" "Correct program with 'repeat' - Euclid algorithm"
run_test "" test003 "0" "" "Correct program with 'repeat' - Euclid algorithm"

echo 
echo -e "  ${red}ELSEIF${NC}"
echo

echo "23" | run_test "" test050 "0" "0" "Correct program - Input 23"
echo "-24" | run_test "" test050 "0" "1" "Correct program - Input -24"
echo "0" | run_test "" test050 "0" "2" "Correct program - Input 0"
run_test "" test051 "0" "" "Correct program"
run_test "" test261 "2" "" "Empty body of 'if' statement"
run_test "" test262 "2" "" "Missing semicolon between two 'if' statements"
# <sem pridavejte nove testy>

################################################################################
echo
echo "--------------------------------------------------------------------------------"
echo "Shrnuti:"
echo "Proslo $ok_count testu z $count"
echo "--------------------------------------------------------------------------------"

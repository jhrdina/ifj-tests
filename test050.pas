{------------------------ Rozsirenie ELSEIF ----------------------------------}
{-------------------------- EXIT_CODE 0 --------------------------------------}
{ Program pre otestovanie funkcnosti podmieneneho prikazu bez else vetvy      }

var
	num:integer;
	tmp:integer;

{ Funkcia pre vypocet absolutnej hodnoty }
function gabs(a : integer) : integer;
begin
	if a < 0 then
		begin
			a := a * (0 - 1)
		end
	else
		begin
		end; { if-else }
	gabs := a
end;


{ Funkcia pre vypocet celociselneho podielu cisel }
function gdiv(a : integer; b : integer) : integer;
var sign : integer; tmp : integer; 
begin
	if a * b < 0 then
		begin
			sign := 0 - 1
		end
	else
		begin
			sign := 1
		end;

	a := gabs(a);
	b := gabs(b);

	gdiv := 0;
	tmp := b;

	while tmp <= a do
		begin
			tmp := tmp + b;
			gdiv := gdiv + 1
		end;
	gdiv := gdiv * sign
end;

{ Funkcia pre vypocet modulo }
function gmod(a : integer; b : integer) : integer;
begin
	gmod := gdiv(a, b);
	gmod := a - b * gmod
end;

begin
	write('Zadajte lubovolne cele cislo: ');
	readln(num);
	write(''#10'Vlastnosti zadaneho cisla'#10'');
	write('------------------------------'#10''#10'');

	if num > 0 then
		begin
		  write(' - kladne'#10'')
		end;
	
	if num < 0 then
		begin
		  write(' - zaporne '#10'')
		end;

	if num = 0 then
		begin
		  write(' - nezaporne'#10'')
		end;

	tmp := gmod(num,2);

	if tmp = 0 then
		begin
		  write(' - parne'#10'')
		end;

	if tmp <> 0 then
		begin
		  write(' - neparne '#10'')
		end
end.

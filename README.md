# Neoficiální testy k projektu IFJ14

## Disclaimer

Testy nejsou kompletní a pravděpodobně vám nezaručí 100% hodnocení.
Pokud najdete nějaké chyby nebo by se někdo chtěl aktivně podílet na jejich vylepšování, neváhejte napsat Honzovi nebo Petrovi (viz Kontakt).

## Spuštění testů

Ideální je naklonovat si repozitář vedle složky s projektem:

    git clone https://jhrdka@bitbucket.org/jhrdka/ifj-tests.git

aby adresářová struktura vypadala zhruba takto:

    ifj-tests
     |- test.sh
     \- ...
    ifj
     |- Makefile
     |- soubory.h
     |- soubory.c
     \- binarka

Na začátku skriptu `test.sh` v proměnné BINARY můžete upravit cestu k binárce.

Skript můžete spustit buďto ve složce s testy nebo i ve složce s projektem (pokud je struktura a pojmenování přesně jako výše). Např.:

    ../ifj-tests/test.sh

by se snad dalo přidat i do cíle `make test` v Makefile.

## Testy scanneru

Nelekněte se, že vám pravděpodobně nebudou fungovat samostatné testy scanneru. S parametrem `-t` nám program pouze vypisuje tokeny a jejich hodnoty (viz .out soubory), případně detekuje Lex. chyby (exit kód 1).

## Rozšíření

V některých testech počítáme s implementací rozšíření **FUNEXP**. Takové testy jsme zatím nijak neoddělovali od ostatních, takže můžou hlásit chyby.

Dále jsou přiloženy i testy pro **REPEAT**, ty už se dají snadno zakomentovat v souboru `test.sh`.

## Chybné použití vestavěných funkcí

Při pokusu o předefinování vestavěných funkcí, případně jejich použití jako proměnné, počítáme s vyhozením sémantické chyby (3 a 4). Podle [fóra](https://wis.fit.vutbr.cz/FIT/st/phorum-msg-show.php?id=37614) je ale přípustná i varianta s lexikální chybou, oficiální testy v tomto prý budou shovívavé.

## Konvence názvů testů

Nové soubory s testy pojmenováváme tak, aby počáteční číslice souhlasily s návratovými kódy.

## Kontakt

Jan Hrdina <xhrdin10@stud.fit.vutbr.cz> (též FB)

Peter Gazdík <xgazdi03@stud.fit.vutbr.cz> (též FB)

## Autoři

 - Jan Hrdina
 - Peter Gazdík
 - Peter Hnat
 - Filip Pokorný
 - Štěpán Granát